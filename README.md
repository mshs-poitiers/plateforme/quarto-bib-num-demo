# Quarto Bib Num Demo
*David Chesnet, Michael Nauge. Univ. Poitiers*

## Objectifs

Présenter le résultat de deploiement d'un site web *bibliothèque numérique* généré par le programme [nakala-quarto-view](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view)

[Consulter le site web de démo](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo)

## Exemples

- Exemple de visualisation d'un [lot d'images](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/dataPost/10_34847-nkl_ffabdg38/10_34847-nkl_25f31n26.html) utilisant le end-point IIIF

- Exemple de [spatialisation](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/collectionPost/10_34847-nkl_ffabdg38.html) via leaflet utilisant le dcterms:spatial.

- Exemple de [visualisation de statistiques](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/collectionPost/stats/10_34847-nkl_ffabdg38.stats.html) via plotlyExpress.
